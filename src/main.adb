with Grid;
with Printer;
with STM32.Board;           use STM32.Board;
with HAL.Bitmap;            use HAL.Bitmap;
with HAL.Framebuffer;
pragma Warnings (Off, "referenced");
with HAL.Touch_Panel;       use HAL.Touch_Panel;
with STM32.User_Button;     use STM32;
with STM32.RNG.Interrupts;
with BMP_Fonts;
with LCD_Std_Out;
with HAL;
with Case_Selection; use Case_Selection;
procedure main is
    G : Grid.Grid := Grid.Random_Grid;
   BG : Bitmap_Color := Blue;
   L, C, FL, FC: Grid.Grid_Index;
   type State is (Pick_First, Pick_Second);
   Game_Status : State := Pick_First;
   Counter : Integer := 0;
begin
    STM32.RNG.Interrupts.Initialize_RNG;
    -- Initialize LCD
    Display.Initialize;
    Display.Initialize_Layer (1, ARGB_8888);

    -- Initialize touch panel
    Touch_Panel.Initialize;

    -- Initialize button
    User_Button.Initialize;

    LCD_Std_Out.Set_Orientation(HAL.Framebuffer.Landscape);
   LCD_Std_Out.Set_Font (BMP_Fonts.Font8x8);
   LCD_Std_Out.Clear_Screen;
   loop
      declare
         State : constant TP_State := Touch_Panel.Get_All_Touch_Points;
      begin
         delay 0.1;
         if Grid.Is_Game_Ended(G) then
            exit;
         end if;
         Printer.Print_Grid(G);
         if Get_Selected_Case(State, FL, FC) and then not Grid.Is_Revealed(G, FL, FC) then
            Grid.Reveal(G, FL, FC);
            if Game_Status = Pick_First then
               L := FL;
               C := FC;
               Game_Status := Pick_Second;
            else
               Game_Status := Pick_First;
               Counter := Counter + 1;
               if Grid.Get_Color(G, L, C) /= Grid.Get_Color(G, FL, FC) then
                  Printer.Print_Grid(G);
                  Grid.Unreveal(G, L, C);
                  Grid.Unreveal(G, FL, FC);
                  delay 0.5;
               end if;
            end if;
         end if;
      end;
   end loop;
   Printer.Print_Victory(Counter);
   loop
      null;
   end loop;
end main;

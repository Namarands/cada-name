with Grid;
with HAL; use HAL;
package Printer is
   procedure Print_Grid(G : in Grid.Grid);
    procedure print_victory(I : Integer);
    Case_Width : constant := 320 / Grid.Grid_Index'Last;
    Case_Height : constant := 240 / Grid.Grid_Index'Last;
end Printer;

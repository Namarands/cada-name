With HAL; use HAL;
with HAL.Bitmap; use HAL.Bitmap;

package Grid is
    type Status is (Revealed, Hidden);
    subtype Grid_Index is UInt32 range 1 .. 4;
    type Words is array (UInt32 range <>) of String(1 .. 1);
    type Grid_Cell is record
        Cell_Color : Bitmap_Color;
        Cell_Status : Status;
    end record;
    type Grid is array (Grid_Index, Grid_Index) of Grid_Cell;
    type Colors is array(UInt32 range <>) of Bitmap_Color;

    function Random_Grid return Grid;
    function Get_At(G : in Grid;
                    Line : in Grid_Index;
                    Column : in Grid_Index) return Grid_Cell
        with Post => Get_At'Result = G(Line, Column);
    function Get_Color(G : in Grid;
                      Line : in Grid_Index;
                      Column : in Grid_Index) return Bitmap_Color
        with Post => Get_Color'Result = G(Line, Column).Cell_Color;
    function Get_Color(C : in Grid_Cell) return Bitmap_Color
        with Post => Get_Color'Result = C.Cell_Color
                    and then (Get_Color(C) = Get_Color(C'Old));

    function Get_Status(C : in Grid_Cell) return Status
        with Post => Get_Status'Result = C.Cell_Status
                    and then (Get_Status(C) = Get_Status(C'Old));

    function Get_Status(G : in Grid;
                      Line : in Grid_Index;
                      Column : in Grid_Index) return Status
        with Post => Get_Status'Result = G(Line, Column).Cell_Status
                    and then (Get_Status(G, Line, Column) = Get_Status(G'Old, Line, Column));

    function Is_Revealed(G : in Grid;
                         Line : in Grid_Index;
                         Column : in Grid_Index) return Boolean
        with Post => Is_Revealed'Result = (Get_Status(G, Line, Column) = Revealed)
                    and then (Get_Status(G, Line, Column) = Get_Status(G'Old, Line, Column));

    function Is_Revealed(C : in Grid_Cell) return Boolean
        with Post => Is_Revealed'Result = (Get_Status(C) = Revealed);
    procedure Reveal(G : in out Grid;
                     Line : in Grid_Index;
                     Column : in Grid_Index)
        with Post => G(Line, Column).Cell_Status = Revealed;
    procedure Reveal(C : in out Grid_Cell)
        with Post => C.Cell_Status = Revealed;

    function Is_Game_Ended(G : in Grid) return Boolean;
   procedure Unreveal(G : in out Grid;
                     Line : in Grid_Index;
                     Column : in Grid_Index);
private
   Available_Color : Colors := (
        Blue,
        Yellow,
        Green,
        Black,
        Brown,
        Purple,
        Red,
        Dark_Blue,
        Magenta,
        Cyan,
        Dark_Salmon
  );
end Grid;

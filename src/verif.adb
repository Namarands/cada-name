with STM32.RNG.Interrupts; use STM32.RNG.Interrupts;
With HAL; use HAL;
with LCD_Std_Out;
package body Verif is
    function Get_Random return UInt32 is
        R : UInt32 := Random;
    begin
        R := R mod (Max - Min + 1);
        R := R + Min;
        return R;
   end Get_Random;

    function Unique_Random(List : in out Verif_List) return UInt32 is
        V : UInt32;
        Same : Boolean := False;
    begin
        Initialize_RNG;
        V := Get_Random;
        Find_Unused_Value:
      loop
            for I in Verif_List_Array'First .. List.Idx loop
                if List.Arr(I) = V then
                    Same := True;
                end if;
            end loop;
            exit Find_Unused_Value when not Same;
            V := Get_Random;
            Same := False;
        end loop Find_Unused_Value;
        List.Idx := List.Idx + 1;
      List.Arr(List.Idx) := V;
        return V;
    end Unique_Random;

    function Get_Array(List : in Verif_List) return Verif_List_Array is
    begin
        return List.Arr;
    end Get_Array;
end Verif;

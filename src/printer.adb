With Grid;
with STM32.Board; use STM32.Board;
with HAL.Bitmap; use HAL.Bitmap;
pragma Warnings (Off, "referenced");
with BMP_Fonts;
with LCD_Std_Out;
with Bitmapped_Drawing; use Bitmapped_Drawing;

package body Printer is
    function Compute_Rect(Line : in Grid.Grid_Index; Column : in Grid.Grid_Index) return Rect is
        P : Point := (
            Y => Integer((Line - 1) * Case_Height),
            X => Integer((Column - 1) * Case_Width)
            );
        R : HAL.Bitmap.Rect := (
            Position => P,
            Width => Case_Width,
            Height => Case_Height
            );
    begin
        return R;
    end Compute_Rect;

    procedure Print_Cell(G : in Grid.Grid; Line : in Grid.Grid_Index; Column : in Grid.Grid_Index) is
        C : Grid.Grid_Cell := Grid.Get_At(G, Line, Column);
        R : Rect := Compute_Rect(Line, Column);
   begin
        if Grid.Is_Revealed(C) then
         Display.Hidden_Buffer(1).Set_Source(Grid.Get_Color(C));
      else
         Display.Hidden_Buffer(1).Set_Source(HAL.Bitmap.White);
         end if;
        Display.Hidden_Buffer(1).Fill_Rect(R);
    end Print_Cell;

    procedure Print_Grid(G : in Grid.Grid) is
    begin
        for L in Grid.Grid_Index'First .. Grid.Grid_Index'Last loop
            for C in Grid.Grid_Index'First .. Grid.Grid_Index'Last loop
                Print_Cell(G, L, C);
            end loop;
      end loop;
      Display.Update_Layer (1, Copy_Back => True);
   end Print_Grid;
   
   procedure print_victory(I : Integer) is
        F : BMP_Fonts.BMP_Font := BMP_Fonts.Font16x24;
        Fg : HAL.Bitmap.Bitmap_Color := White;
        Bg : HAL.Bitmap.Bitmap_Color := Black;
    begin
        Display.Hidden_Buffer(1).Set_Source(Black);
        Display.Hidden_Buffer(1).Fill;
      Draw_String(Display.Hidden_Buffer(1).all, (X => 5, Y => 120), "You won in" & I'Image & " turns!" , F, Fg, Bg);
      Display.Update_Layer (1, Copy_Back => True);
      end print_victory;
end Printer;

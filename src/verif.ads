With Ada.Numerics.Discrete_Random;
With HAL; use HAL;

generic
Size : UInt32;
Min : UInt32;
Max : UInt32;
package Verif is
    type Verif_List is private;
    type Verif_List_Array is array (1 .. Size) of UInt32;

    function Unique_Random(List : in out Verif_List) return UInt32
        with Post => (for all E of Get_Array(List'Old) => E /= Unique_Random'Result)
                    and then (for some E of Get_Array(List) => E = Unique_Random'Result);

    function Get_Array(List : in Verif_List) return Verif_List_Array;
private
    type Verif_List is record
        Idx : UInt32 := 0;
        Arr : Verif_List_Array;
    end record;

end Verif;

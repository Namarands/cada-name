With Ada.Numerics.Discrete_Random;
with HAL;                               use HAL;
with Verif;

package body Grid is
    procedure Set_At(G : in out Grid;
        Line : in Grid_Index;
        Column : in Grid_Index;
        Cell : in Grid_Cell) is
    begin
        G(Line, Column) := Cell;
    end Set_At;

    procedure Add_Colors(G : in out Grid) is
        subtype Case_Position is UInt32 range 1 .. (Grid_Index'Last * Grid_Index'Last);
        package Verif_Word_Range is new Verif(Min => Case_Position'First,
        Max => Case_Position'Last,
        Size => Case_Position'Last);
        use Verif_Word_Range;
        List : Verif_List;
        procedure Set_Color(G : in out Grid;
            I : Uint32;
            Cell_Color : in Bitmap_Color) is
            Line : Grid_Index := (I - 1) / Grid_Index'Last + 1;
            Column : Grid_Index := (I - 1) mod Grid_Index'Last + 1;
            Cell : Grid_Cell := Get_At(G, Line, Column);
        begin
            Cell.Cell_Color := Cell_Color;
            Set_At(G, Line, Column, Cell);
        end Set_Color;
    begin
        for I in 1 .. Case_Position'Last / 2 loop
            Set_Color(G, Unique_Random(List), Available_Color(I));
            Set_Color(G, Unique_Random(List), Available_Color(I));
        end loop;
    end Add_Colors;

    function Random_Grid return Grid is
        G : Grid := (others => (others => (
            Cell_Color => White,
            Cell_Status => Hidden
            )));
    begin
        Add_Colors(G);
        return G;
    end Random_Grid;

    function Get_At(G : in Grid;
        Line : in Grid_Index;
        Column : in Grid_Index) return Grid_Cell is
    begin
        return G(Line, Column);
    end Get_At;

    function Get_Color(G : in Grid;
        Line : in Grid_Index;
        Column : in Grid_Index) return Bitmap_Color is
        Cell : Grid_Cell := Get_At(G, Line, Column);
    begin
        return Get_Color(Cell);
    end Get_Color;

    function Get_Color(C : in Grid_Cell) return Bitmap_Color is
    begin
        return C.Cell_Color;
    end Get_Color;

    function Get_Status(C : in Grid_Cell) return Status is
    begin
        return C.Cell_Status;
    end Get_Status;

    function Get_Status(G : in Grid;
                      Line : in Grid_Index;
                      Column : in Grid_Index) return Status is
    begin
        return G(Line, Column).Cell_Status;
    end Get_Status;

    function Is_Revealed(G : in Grid;
        Line : in Grid_Index;
        Column : in Grid_Index) return Boolean is
        Cell : Grid_Cell := Get_At(G, Line, Column);
    begin
        return Is_Revealed(Cell);
    end Is_Revealed;

    function Is_Revealed(C : in Grid_Cell) return Boolean is
    begin
        return C.Cell_Status = Revealed;
    end Is_Revealed;

    procedure Reveal(G : in out Grid;
        Line : in Grid_Index;
        Column : in Grid_Index) is
        Cell : Grid_Cell := Get_At(G, Line, Column);
    begin
        Reveal(Cell);
        Set_At(G, Line, Column, Cell);
   end Reveal;

    procedure Unreveal(G : in out Grid;
        Line : in Grid_Index;
        Column : in Grid_Index) is
        Cell : Grid_Cell := Get_At(G, Line, Column);
    begin
        Cell.Cell_Status := Hidden;
        Set_At(G, Line, Column, Cell);
    end Unreveal;

    procedure Reveal(C : in out Grid_Cell) is
    begin
        C.Cell_Status := Revealed;
    end Reveal;

    function Is_Game_Ended(G : in Grid) return Boolean is
    begin
        for Line in Grid_Index'First .. Grid_Index'Last loop
            for Column in Grid_Index'First .. Grid_Index'Last loop
                if G(Line, Column).Cell_Status = Hidden then
                    return False;
                end if;
            end loop;
        end loop;

        return True;
    end Is_Game_Ended;

end Grid;

With Ada.Numerics.Discrete_Random;
With HAL; use HAL;

generic
Size : UInt32;
Min : UInt32;
Max : UInt32;
package Verif is
    type Verif_List is private;

    function Unique_Random(List : in out Verif_List) return UInt32;

    private

    type Verif_List_Array is array (1 .. Size) of UInt32;

    type Verif_List is record
        Idx : UInt32 := 0;
        Arr : Verif_List_Array;
    end record;

end Verif;

With HAL;                       use HAL;
with Message_Buffers;           use Message_Buffers;

package Grid is
    type Color is (Blue, Red, White, Black);
    subtype Player_Color is Color range Blue .. Red;
    type Status is (Revealed, Hidden);
    subtype Grid_Index is UInt32 range 1 .. 4;
    subtype First_Player_Hidden_Number is UInt32 range 1 .. 4;
    subtype Second_Player_Hidden_Number is First_Player_Hidden_Number range 1 .. First_Player_Hidden_Number'Last - 1;
    type Grid_Cell is private;
    type Grid is private;
    function Empty_Grid return Grid;
    function "not"(C : Player_Color) return Player_Color;
    function Random_Grid(First_Player : out Player_Color) return Grid;
    function Get_At(G : in Grid;
                    Line : in Grid_Index;
                    Column : in Grid_Index) return Grid_Cell;
    function Get_Color(G : in Grid;
                       Line : in Grid_Index;
                       Column : in Grid_Index) return Color;
    function Get_Color(C : in Grid_Cell) return Color;
    procedure Set_Color(G : in out Grid;
                        Line : in Grid_Index;
                        Column : in Grid_Index;
                        C : in Color);
    function Get_Word(G : in Grid;
                      Line : in Grid_Index;
                      Column : in Grid_Index) return String;
    function Get_Word(C : in Grid_Cell) return String;
    procedure Set_Word(G : in out Grid;
                      Line : in Grid_Index;
                      Column : in Grid_Index;
                      Idx : in Integer);
    function Is_Revealed(G : in Grid;
                         Line : in Grid_Index;
                         Column : in Grid_Index) return Boolean;
    function Is_Revealed(C : in Grid_Cell) return Boolean;
    procedure Reveal(G : in out Grid;
                     Line : in Grid_Index;
                     Column : in Grid_Index);
    procedure Reveal(C : in out Grid_Cell);
    procedure Print(G : in Grid);
    procedure Print_Spy(G : in Grid);
    procedure Switch_Player(P : in out Player_Color);
    function Spy_Turn(P : in Player_Color) return Positive;
    procedure Team_Turn(P : in Player_Color; G : in out Grid;
                        Nb_Turn : in Positive; Playing : out Boolean);
    function Is_Game_Ended(G : in Grid; P : in Player_Color; C : in Color) return Boolean;

    procedure Construct_Grid_Message(M : out Message; G : in Grid);
    procedure Set_New_Grid(M : in Message; G : out Grid);

private
    type Grid_Data is record
        Red_Total : UInt32 := 0;
        Red_Revealed : UInt32 := 0;
        Blue_Total : UInt32 := 0;
        Blue_Revealed : UInt32 := 0;
    end record;
    type Words is array (UInt32 range <>) of String(1 .. 1);
    type Grid is array (Grid_Index, Grid_Index) of Grid_Cell;
    type Grid_Cell is record
        Idx : UInt32;
        Cell_Color : Color;
        Cell_Status : Status;
    end record;

    -- Instanciate one for the game
    G_Data : Grid_Data;

    --  Amount : UInt32 := 26;
    W : Words := (
            "a", "b", "c", "d", "e",
            "f", "g", "h", "i", "j",
            "k", "l", "m", "n", "o",
            "p", "q", "r", "s", "t",
            "u", "v", "w", "x", "y", "z"
            );
    --subtype Word_Range is UInt32 range 1 .. Words'Length;--UInt32'First .. Amount - 1;

        
    end Grid;

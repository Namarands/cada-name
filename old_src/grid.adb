With Ada.Numerics.Discrete_Random;
with Ada.Characters.Latin_1;            use Ada.Characters.Latin_1;
with Ada.Text_IO;                       use Ada.Text_IO;
with Communication;
with HAL;                               use HAL;
with LCD_Std_Out;
with Verif;
with STM32.RNG.Interrupts; use STM32.RNG.Interrupts;

package body Grid is
    function Get_Color_Escape(C : in Color) return String is
        Color_Escape : String (1 .. 4);
    begin
        case C is
            when Red => Color_Escape := "[91m";
            when Blue => Color_Escape :=  "[34m";
            when Black => Color_Escape :=  "[90m";
            when White => Color_Escape :=  "[97m";
        end case;
        return Color_Escape;
    end Get_Color_Escape;

    procedure Set_At(G : in out Grid;
        Line : in Grid_Index;
        Column : in Grid_Index;
        Cell : in Grid_Cell) is
    begin
        G(Line, Column) := Cell;
    end Set_At;

    function Choose_First return Player_Color is
    begin
        if Random mod 2 = 1 then
            return Blue;
        else
            return Red;
        end if;
    end Choose_First;

    function "not"(C : Player_Color) return Player_Color is
    begin
        case C is
            when Red => return Blue;
            when Blue => return Red;
        end case;
    end "not";

    procedure Add_Colors(G : in out Grid; First : Player_Color) is
        subtype Case_Position is UInt32 range 1 .. Grid_Index'Last * Grid_Index'Last;
        package Verif_Word_Range is new Verif(Min => Case_Position'First,
        Max => Case_Position'Last,
        Size => First_Player_Hidden_Number'Last + Second_Player_Hidden_Number'Last + 1);
        use Verif_Word_Range;
        List : Verif_List;
        Position : Case_Position;
        procedure Set_Color(G : in out Grid;
            Line : in Grid_Index;
            Column : in Grid_Index;
            Cell_Color : in Color) is
            Cell : Grid_Cell := Get_At(G, Line, Column);
        begin
            Cell.Cell_Color := Cell_Color;
            Set_At(G, Line, Column, Cell);
        end Set_Color;
    begin
        if First = Red then
            G_Data.Red_Total := First_Player_Hidden_Number'Last - First_Player_Hidden_Number'First + 1;
            G_Data.Blue_Total := Second_Player_Hidden_Number'Last - Second_Player_Hidden_Number'First + 1;
        else
            G_Data.Blue_Total := First_Player_Hidden_Number'Last - First_Player_Hidden_Number'First + 1;
            G_Data.Red_Total := Second_Player_Hidden_Number'Last - Second_Player_Hidden_Number'First + 1;
        end if;

        -- Player 1
        for I in First_Player_Hidden_Number'First .. First_Player_Hidden_Number'Last loop
            Position := Unique_Random(List);
            Set_Color(G, Position mod Grid_Index'Last + 1, Position / Grid_Index'Last + 1, First);
        end loop;
        for I in Second_Player_Hidden_Number'First .. Second_Player_Hidden_Number'Last loop
            Position := Unique_Random(List);
            Set_Color(G, Position mod Grid_Index'Last + 1, Position / Grid_Index'Last + 1, not First);
        end loop;
        Position := Unique_Random(List);
        Set_Color(G, Position mod Grid_Index'Last + 1, Position / Grid_Index'Last + 1, Black);
    end Add_Colors;

    function Empty_Grid return Grid is
        C : Grid_Cell := (
            Idx => 0,
            Cell_Color => White,
            Cell_Status => Hidden
            );
        G : Grid := (others => (others => C));
    begin
        Add_Colors(G, Red);
        return G;
    end Empty_Grid;

    function Random_Grid(First_Player : out Player_Color) return Grid is
--        package Verif_Word_Range is new Verif(Min => Word_Range'First,
--        Max => Word_Range'Last,
--        Size => Grid_Index'Last * Grid_Index'Last);
        package Verif_Word_Range is new Verif(Min => W'First, Max => W'Last,
                                  Size => Grid_Index'Last * Grid_Index'Last);
        use Verif_Word_Range;
        List : Verif_List;

        function Random_Cell return Grid_Cell is
            Idx : UInt32 := Unique_Random(List);
            C : Grid_Cell := (
                Idx => Idx,
                Cell_Color => White,
                Cell_Status => Hidden);
        begin
            return C;
        end Random_Cell;

        G : Grid := (others => (others => Random_Cell));
    begin
        First_Player := Choose_First;
        Add_Colors(G, First_Player);
        return G;
    end Random_Grid;

    function Get_At(G : in Grid;
        Line : in Grid_Index;
        Column : in Grid_Index) return Grid_Cell is
    begin
        return G(Line, Column);
    end Get_At;

    function Get_Color(G : in Grid;
        Line : in Grid_Index;
        Column : in Grid_Index) return Color is
        Cell : Grid_Cell := Get_At(G, Line, Column);
    begin
        return Get_Color(Cell);
    end Get_Color;

    function Get_Color(C : in Grid_Cell) return Color is
    begin
        return C.Cell_Color;
    end Get_Color;

    function Get_Word(G : in Grid;
        Line : in Grid_Index;
        Column : in Grid_Index) return String is
        Cell : Grid_Cell := Get_At(G, Line, Column);
    begin
        return Get_Word(Cell);
    end Get_Word;

    function Get_Word(C : in Grid_Cell) return String is
    begin
        return W(C.Idx);
    end Get_Word;

    function Is_Revealed(G : in Grid;
        Line : in Grid_Index;
        Column : in Grid_Index) return Boolean is
        Cell : Grid_Cell := Get_At(G, Line, Column);
    begin
        return Is_Revealed(Cell);
    end Is_Revealed;

    function Is_Revealed(C : in Grid_Cell) return Boolean is
    begin
        case C.Cell_Status is
            when Revealed => return True;
            When others => return False;
        end case;
    end Is_Revealed;

    procedure Reveal(G : in out Grid;
        Line : in Grid_Index;
        Column : in Grid_Index) is
        Cell : Grid_Cell := Get_At(G, Line, Column);
    begin
        Reveal(Cell);
        Set_At(G, Line, Column, Cell);
    end Reveal;

    procedure Reveal(C : in out Grid_Cell) is
    begin
        C.Cell_Status := Revealed;
    end Reveal;

    procedure Print_Cell(C : Grid_Cell) is
        Color_Escape : String(1 .. 4);
    begin
        if Is_Revealed(C) then
            Color_Escape := Get_Color_Escape(C.Cell_Color);
        else
            Color_Escape := "[97m";
        end if;
        Put(ESC & Color_Escape & Get_Word(C) & ESC & "[0m");
    end Print_Cell;

    procedure Print_Cell_Spy(C : Grid_Cell) is
        Color_Escape : String(1 .. 4);
    begin
        Color_Escape := Get_Color_Escape(C.Cell_Color);
        Put(ESC & Color_Escape & Get_Word(C) & ESC & "[0m");
    end Print_Cell_Spy;


    procedure Print(G : in Grid) is
    begin
        for Line in Grid_Index'First .. Grid_Index'Last loop
            for Column in Grid_Index'First .. Grid_Index'Last loop
                Put('|');
                Print_Cell(Get_At(G, Line, Column));
            end loop;
            Put_Line("|");
        end loop;
    end Print;

    procedure Print_Spy(G : in Grid) is
    begin
        for Line in Grid_Index'First .. Grid_Index'Last loop
            for Column in Grid_Index'First .. Grid_Index'Last loop
                Put('|');
                Print_Cell_Spy(Get_At(G, Line, Column));
            end loop;
            Put_Line("|");
        end loop;
    end Print_Spy;

    function Spy_Turn(P : in Player_Color) return Positive is
        Color_Escape : String(1 .. 4);
        S : String (1 .. 2) := (others => Character'Val(0));
        -- N : Natural;
        Nb_Turn : Positive := 1;
    begin
        case P is
            when Red => Color_Escape := "[91m";
            when Blue => Color_Escape :=  "[34m";
        end case;
        Put_Line("Spy " & ESC & Color_Escape & Player_Color'Image(P)
        & ESC & "[0m" & "'s turn");
        -- Input there <3 Get_Line(S, N);
        return Positive'Value(S (1 .. 1));
    end Spy_Turn;

    procedure Team_Turn(P : in Player_Color; G : in out Grid;
        Nb_Turn : in Positive; Playing : out Boolean) is
        Color_Escape : String(1 .. 4) := (others => Character'Val(0));
        S_Line : String (1 .. 2) := (others => Character'Val(0));
        S_Col : String (1 .. 2) := (others => Character'Val(0));
        Line : Grid_Index;
        Col : Grid_Index;
        -- N : Natural;
        Idx : Natural := Nb_Turn;
        Cell_Color : Color;
    begin
        Color_Escape := Get_Color_Escape(P);
        Put_Line("Team " & ESC & Color_Escape & Player_Color'Image(P) & ESC
                    & "[0m" & "'s turn");

        while Idx /= 0 loop
            Put("Line: ");
            -- Get_Line(S_Line, N);
            Line := Grid_Index'Value(S_Line (1 .. 1));

            Put("Column: ");
            -- Get_Line(S_Col, N);
            Col := Grid_Index'Value(S_Col (1 .. 1));

            if Is_Revealed(G, Line, Col) then
                Put_Line("This word has already been revealed, please pick another one.");
            else
                Reveal(G, Line, Col);
                Cell_Color := Get_Color(G, Line, Col);
                Playing := not (Cell_Color = Black);

                if Cell_Color /= P  and then Cell_Color /= Black then
                    Put_Line("You found a " & Color'Image(Cell_Color) & " cell. This stops your turn.");
                else
                    Put_Line("You found a good cell!");
                end if;

                Playing := Playing and then not Is_Game_Ended(G, P, Cell_Color);
                if not Playing or else Cell_Color /= P then
                    exit;
                end if;
                Idx := Idx - 1;
            end if;
        end loop;
        Communication.Spy_Continue;

    end Team_Turn;

    procedure Switch_Player(P : in out Player_Color) is
    begin
        P := not P;
    end Switch_Player;

    function Is_Game_Ended(G : in Grid; P : in Player_Color; C : in Color) return Boolean is
    begin
        if C = Black then
            Put_Line("You found the BLACK cell, you lost.");
            Put_Line(ESC & Get_Color_Escape(not P) & Player_Color'Image(not P) & ESC & "[0m" & " team won!");
            Communication.Spy_Game_Over;
            return True;
        end if;
        if C = Red then
            G_Data.Red_Revealed := G_Data.Red_Revealed + 1;
            if G_Data.Red_Total = G_Data.Red_Revealed then
                Put_Line(ESC & Get_Color_Escape(Red) & Player_Color'Image(Red) & ESC & "[0m" & " team won!");
                return True;
            end if;
        elsif C = Blue then
            G_Data.Blue_Revealed := G_Data.Blue_Revealed + 1;
            if G_Data.Blue_Total = G_Data.Blue_Revealed then
                Put_Line(ESC & Get_Color_Escape(Blue) & Player_Color'Image(Blue) & ESC & "[0m" & " team won!");
                return True;
            end if;
        end if;
        return False;
    end Is_Game_Ended;

    procedure Construct_Grid_Message(M : out Message; G : in Grid) is
        String_Size : constant Integer := Integer(Grid_Index'Last) * Integer(Grid_Index'Last) * 2;
        Message_Content : String (1 .. String_Size);
        Current : Grid_Cell;
        Idx : Integer := Message_Content'First;
    begin
        for Line in Grid_Index'First .. Grid_Index'Last loop
            for Column in Grid_Index'First .. Grid_Index'Last loop
                Current := Get_At(G, Line, Column);
                Message_Content(Idx) := Character'Val(Integer(Current.Idx));
                Message_Content(Idx + 1) := Character'Val(Color'Pos(Current.Cell_Color));
                Idx := Idx + 2;
            end loop;
        end loop;

        Message_Buffers.Set(M, Message_Content);

    end Construct_Grid_Message;

    procedure Set_New_Grid(M : in Message; G : out Grid) is
        Message_Content : String := Content(M);
        Idx : Integer := Message_Content'First;
        C : Color;
        Word_Idx : Integer;
    begin
        for Line in Grid_Index'First .. Grid_Index'Last loop
            for Column in Grid_Index'First .. Grid_Index'Last loop
                Word_Idx := Integer'Value((1 => Message_Content(Idx)));
                C := Color'Val(Integer'Value((1 => Message_Content(Idx + 1))));
                Set_Color(G, Line, Column, C);
                Set_Word(G, Line, Column, Word_Idx);
                Idx := Idx + 2;
            end loop;
        end loop;

 
    end Set_New_Grid;

    procedure Set_Word(G : in out Grid;
                      Line : in Grid_Index;
                      Column : in Grid_Index;
                      Idx : in Integer) is
    begin
        G(Line, Column).Idx := UInt32(Idx);
    end Set_Word;

    procedure Set_Color(G : in out Grid;
                        Line : in Grid_Index;
                        Column : in Grid_Index;
                        C : in Color) is
    begin
        G(Line, Column).Cell_Color := C;
    end Set_Color;
 
end Grid;

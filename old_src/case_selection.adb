with Grid; use Grid;
With HAL; Use HAL;
With Printer;
with LCD_Std_Out;
package body Case_Selection is
   function Get_Selected_Case(State : TP_State; Line, Column : out Grid_Index) return Boolean is
    begin
         case State'Length is
         when 1 =>
            Line := 1 + UInt32(State(State'First).X) / Printer.Case_Height;
            Column := Grid_Index'Last - UInt32(State(State'First).Y) / Printer.Case_Width;
            return True;
            when others => return False;
         end case;
    end Get_Selected_Case;
end Case_Selection;

with printer;
with Grid;

package body Communication is
    procedure Determine_Role(G : in out Grid.Grid; Is_Spy : out Boolean) is
        Button_Is_Pressed : Boolean := False;
        Message_Role : aliased Message (Physical_Size => 1);
        Message_Grid : aliased Message (Physical_Size
            => Integer(Grid.Grid_Index'Last) * Integer(Grid.Grid_Index'Last) * 2);
   begin
        Serial_IO.Blocking.Initialize(Block_Port);
        Serial_IO.Nonblocking.Initialize(Nonblock_Port);

      Serial_IO.Blocking.Configure(Block_Port, Baud_Rate => 9_600);
      Serial_IO.Nonblocking.Configure(Nonblock_Port, Baud_Rate => 9_600);

        Is_Spy := False;
      Serial_IO.Nonblocking.Get(Nonblock_Port, Message_Role'Unchecked_Access);

        while not Button_Is_Pressed and not Serial_IO.Nonblocking.Receiving(Nonblock_Port) loop
            Button_Is_Pressed := STM32.User_Button.Has_Been_Pressed;
        end loop;

        if Button_Is_Pressed then
            -- Construct Message_Role
            Message_Buffers.Set(Message_Role, Val_Role_Spy);
            -- Send it to the Team Board
            Serial_IO.Blocking.Put(Block_Port, Message_Role'Access);
            Is_Spy := True;
            -- Construct Message_Grid
            Grid.Construct_Grid_Message(Message_Grid, G);
            Serial_IO.Blocking.Put(Block_Port, Message_Grid'Access);
        else
            Is_Spy := False;
             -- Wait for message to be complete
            Await_Reception_Complete(Message_Role);
            if Content(Message_Role)(1) = Val_Role_Spy then
                Serial_IO.Blocking.Get(Block_Port, Message_Grid'Access);
                Grid.Set_New_Grid(Message_Grid, G);
            else
                -- Error
                -- TODO ?
                null;
            end if;
           null;
        end if;

    end Determine_Role;

    -- Receive
    procedure Wait_Spy (Playing : out Boolean) is
        Data_Receive : aliased Message (Physical_Size => 1);
    begin
        Serial_IO.Blocking.Get(Block_Port, Data_Receive'Access);
        Playing := (Content(Data_Receive)(1) = Val_Spy_Continue);
    end Wait_Spy;

    procedure Get_Nb_Turn (Nb_Turn : out Positive) is
        Data_Receive : aliased Message (Physical_Size => 2);
    begin
       Serial_IO.Blocking.Get(Block_Port, Data_Receive'Access);
       if Content(Data_Receive)(1) = Team_Nb_Turn then
           Nb_Turn := Positive'Value((1 => Content(Data_Receive)(2)));
       end if;
    end Get_Nb_Turn;

    -- Send
    procedure Spy_Continue is
        To_Send : aliased Message (Physical_Size => 1);
    begin
        Message_Buffers.Set(To_Send, Val_Spy_Continue);
        Serial_IO.Blocking.Put(Block_Port, To_Send'Access);
    end Spy_Continue;

    procedure Spy_Game_Over is
        To_Send : aliased Message (Physical_Size => 1);
    begin
        Message_Buffers.Set(To_Send, Val_Spy_Game_Over);
        Serial_IO.Blocking.Put(Block_Port, To_Send'Access);
    end Spy_Game_Over;

    procedure Send_Number(Nb : in Positive) is
        To_Send : aliased Message (Physical_Size => 2);
    begin
        Message_Buffers.Set(To_Send, Val_Spy_Game_Over & Character'Val(Nb));
        Serial_IO.Blocking.Put(Block_Port, To_Send'Access);
    end Send_Number;

end Communication;

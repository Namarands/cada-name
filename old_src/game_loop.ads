with Grid; Use Grid;

package Game_Loop is
    procedure Play_Spy(G : in Grid.Grid; First : Player_Color);
    procedure Play_Guess(G : in out Grid.Grid; First : Player_Color);
    type Game_Info is private;
private
    type State is (Waiting, Picking);
   type Windows is (Grid_Window, Button_Window);
   type Step is (Continue, Loose, Next);
    type Game_Info is record
      Current_Player : Player_Color;
      First_Player : Player_Color;
        First_Player_Case : First_Player_Hidden_Number := First_Player_Hidden_Number'Last;
        Second_Player_Case : Second_Player_Hidden_Number := Second_Player_Hidden_Number'Last;
        Status : State := Picking;
        Window : Windows := Grid_Window;
    end record;
end Game_Loop;

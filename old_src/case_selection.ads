with Grid; use Grid;
with HAL.Touch_Panel; use HAL.Touch_Panel;

package Case_Selection is
    function Get_Selected_Case(State : TP_State; Line, Column : out Grid_Index) return Boolean;
end Case_Selection;

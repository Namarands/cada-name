with Ada.Interrupts.Names;
with Ada.Interrupts;
with Grid;
with Message_Buffers;           use Message_Buffers;
with STM32.Device;              use STM32.Device;
with STM32.User_Button;
with Serial_IO.Blocking;
with Message_Buffers;           use Message_Buffers;
with HAL;                       use HAL;
with Serial_IO.Nonblocking;

package Communication is

    procedure Determine_Role(G : in out Grid.Grid; Is_Spy : out Boolean);

    -- Receive
    procedure Wait_Spy(Playing : out Boolean);
    procedure Get_Nb_Turn(Nb_Turn : out Positive);

    -- Send
    procedure Spy_Continue;
    procedure Spy_Game_Over;
    procedure Send_Number(Nb : in Positive);

private
    Val_Spy_Continue : constant Character := 'c';
    Val_Spy_Game_Over : constant Character := 'g';
    Team_Nb_Turn : constant Character := 'n'; -- then Nb_Turn in ASCII
    Val_Role_Spy : constant Character := 's';

    Peripheral : aliased Serial_IO.Peripheral_Descriptor :=
            (Transceiver    =>  USART_1'Access,
            Transceiver_AF  =>  GPIO_AF_USART1_7,
            Tx_Pin          =>  PB6,
            Rx_Pin          =>  PB7);
    IRQ : constant Ada.Interrupts.Interrupt_ID := Ada.Interrupts.Names.USART1_Interrupt;

    Block_Port : Serial_IO.Blocking.Serial_Port(Peripheral'Access);
    Nonblock_Port : Serial_IO.Nonblocking.Serial_Port(IRQ, Peripheral'Access);

end Communication;

with Grid;
with Printer;
with Game_Loop;
With Case_Selection; use Case_Selection;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;
with Communication;

with STM32.Board;           use STM32.Board;
with HAL.Bitmap;            use HAL.Bitmap;
with HAL.Framebuffer;
pragma Warnings (Off, "referenced");
with HAL.Touch_Panel;       use HAL.Touch_Panel;
with STM32.User_Button;     use STM32;
with STM32.RNG.Interrupts;
with BMP_Fonts;
with LCD_Std_Out;
with HAL;
procedure main is
    P : Grid.Player_Color;
    G : Grid.Grid := Grid.Random_Grid(P);
    BG : Bitmap_Color := (Alpha => 255, others => 0);
    I : HAL.Uint32;
    Playing : Boolean := True;
    Is_Spy : Boolean;
begin
    STM32.RNG.Interrupts.Initialize_RNG;
    I := STM32.RNG.Interrupts.Random;

    -- Initialize LCD
    Display.Initialize;
    Display.Initialize_Layer (1, ARGB_8888);

    -- Initialize touch panel
    Touch_Panel.Initialize;

    -- Initialize button
    User_Button.Initialize;

    LCD_Std_Out.Set_Orientation(HAL.Framebuffer.Landscape);
    LCD_Std_Out.Set_Font (BMP_Fonts.Font8x8);

   LCD_Std_Out.Clear_Screen;
    Printer.Print_Home(P);
    Display.Update_Layer (1, Copy_Back => True);

    -- Determine who is Is_Spy and who is Slave
    Communication.Determine_Role(G, Is_Spy);

   if Is_Spy then
      Printer.Print_Start_Spy(P);
      delay 2.0;
        Game_Loop.Play_Spy(G, P);
   else
      Printer.Print_Start_Guess(P);
      delay 2.0;
        Game_Loop.Play_Guess(G, P);
    end if;

end main;

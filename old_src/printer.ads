with Grid;
with Game_Loop;
with HAL; use HAL;
package Printer is
    procedure Print_Grid(G : in Grid.Grid);
    procedure Print_Grid_Spy(G : in Grid.Grid);
   procedure Print_Selection(P : Grid.Player_Color; Current : UInt32);
   procedure Print_Waiter(P : Grid.Player_Color; Picked : UInt32);
   procedure Print_Waiter(P : Grid.Player_Color);
   procedure Win_Screen(P : Grid.Player_Color);
   procedure Loose_Screen(P : Grid.Player_Color);
   procedure Print_Home(P : Grid.Player_Color);
   procedure Print_Start_Spy(P : Grid.Player_Color);
   procedure Print_Start_Guess(P : Grid.Player_Color);
    Case_Width : constant := 320 / Grid.Grid_Index'Last;
    Case_Height : constant := 240 / Grid.Grid_Index'Last;
end Printer;

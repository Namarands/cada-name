With Grid;
with STM32.Board; use STM32.Board;
with HAL.Bitmap; use HAL.Bitmap;
pragma Warnings (Off, "referenced");
with BMP_Fonts;
with LCD_Std_Out;
with Bitmapped_Drawing; use Bitmapped_Drawing;

package body Printer is
    function "not"(C : Grid.Color) return Grid.Color is
    begin
        case C is
            when Grid.Blue => return Grid.Black;
            when Grid.Red =>return Grid.Black;
            when Grid.White => return Grid.Black;
            when Grid.Black => return Grid.White;
        end case;
    end "not";

    function Compute_Rect(Line : in Grid.Grid_Index; Column : in Grid.Grid_Index) return Rect is
        P : Point := (
            Y => Integer((Line - 1) * Case_Height),
            X => Integer((Column - 1) * Case_Width)
            );
        R : HAL.Bitmap.Rect := (
            Position => P,
            Width => Case_Width,
            Height => Case_Height
            );
    begin
        return R;
    end Compute_Rect;

    function To_HAL_Color(C : Grid.Color) return HAL.Bitmap.Bitmap_Color is
    begin
        case C is
            when Grid.Blue => return HAL.Bitmap.Blue;
            when Grid.Red =>return HAL.Bitmap.Red;
            when Grid.White => return HAL.Bitmap.White;
            when Grid.Black => return HAL.Bitmap.Black;
        end case;
    end To_HAL_Color;

    procedure Print_String(C : Grid.Grid_Cell; R : Rect; Fg, bg : HAL.Bitmap.Bitmap_Color) is
        F : BMP_Fonts.BMP_Font := BMP_Fonts.Font12x12;
        P : Point := (
            X => R.Position.X,
            Y => R.Position.Y + Case_Height / 3
            );
    begin
        Draw_String(Display.Hidden_Buffer(1).all, P, Grid.Get_Word(C), F, Fg, Bg);
    end Print_String;

    procedure Print_String(C : Grid.Grid_Cell; R : Rect) is
        Fg : HAL.Bitmap.Bitmap_Color := To_HAL_Color(not Grid.Get_Color(C));
        Bg : HAL.Bitmap.Bitmap_Color := To_HAL_Color(Grid.Get_Color(C));
    begin
        Print_String(C, R, Fg, Bg);
    end Print_String;

    procedure Print_Cell_Revealed(G : in Grid.Grid; Line : in Grid.Grid_Index; Column : in Grid.Grid_Index) is
        C : Grid.Grid_Cell := Grid.Get_At(G, Line, Column);
        R : Rect := Compute_Rect(Line, Column);
    begin
        Display.Hidden_Buffer(1).Set_Source(To_HAL_Color(Grid.Get_Color(C)));
        Display.Hidden_Buffer(1).Fill_Rect(R);
        Print_String(C, R);
    end Print_Cell_Revealed;

    procedure Print_Cell_Hidden(G : in Grid.Grid; Line : in Grid.Grid_Index; Column : in Grid.Grid_Index) is
        C : Grid.Grid_Cell := Grid.Get_At(G, Line, Column);
        R : Rect := Compute_Rect(Line, Column);
    begin
        Display.Hidden_Buffer(1).Set_Source(HAL.Bitmap.Sandy_Brown);
        Display.Hidden_Buffer(1).Fill_Rect(R);
        Print_String(C, R, Hal.Bitmap.Black, HAL.Bitmap.Sandy_Brown);
    end Print_Cell_Hidden;

    procedure Print_Cell_Spy(G : in Grid.Grid; Line : in Grid.Grid_Index; Column : in Grid.Grid_Index) is
        C : Grid.Grid_Cell := Grid.Get_At(G, Line, Column);
        R : Rect := Compute_Rect(Line, Column);
    begin
        if Grid.Is_Revealed(C) then
            Print_Cell_Hidden(G, Line, Column);
        else
            Print_Cell_Revealed(G, Line, Column);
        end if;
   end Print_Cell_Spy;
   
    procedure Print_Cell(G : in Grid.Grid; Line : in Grid.Grid_Index; Column : in Grid.Grid_Index) is
        C : Grid.Grid_Cell := Grid.Get_At(G, Line, Column);
        R : Rect := Compute_Rect(Line, Column);
    begin
        if Grid.Is_Revealed(C) then
            Print_Cell_Revealed(G, Line, Column);
        else
            Print_Cell_Hidden(G, Line, Column);
        end if;
    end Print_Cell;

    procedure Print_Grid(G : in Grid.Grid) is
    begin
        for L in Grid.Grid_Index'First .. Grid.Grid_Index'Last loop
            for C in Grid.Grid_Index'First .. Grid.Grid_Index'Last loop
                Print_Cell(G, L, C);
            end loop;
        end loop;
    end Print_Grid;

    procedure Print_Grid_Spy(G : in Grid.Grid) is
    begin
        for L in Grid.Grid_Index'First .. Grid.Grid_Index'Last loop
            for C in Grid.Grid_Index'First .. Grid.Grid_Index'Last loop
                Print_Cell_Spy(G, L, C);
            end loop;
        end loop;
    end Print_Grid_Spy;

    procedure Print_Waiter(P : Grid.Player_Color; Picked : UInt32) is
        F : BMP_Fonts.BMP_Font := BMP_Fonts.Font16x24;
        Fg : HAL.Bitmap.Bitmap_Color := To_HAL_Color(not P);
        Bg : HAL.Bitmap.Bitmap_Color := To_HAL_Color(P);
    begin
        Display.Hidden_Buffer(1).Set_Source(To_HAL_Color(P));
        Display.Hidden_Buffer(1).Fill;
        Draw_String(Display.Hidden_Buffer(1).all, (X => 100, Y => 120), Picked'Image & " left...", F, Fg, Bg);
   end Print_Waiter;
   
   procedure Print_Waiter(P : Grid.Player_Color) is
        F : BMP_Fonts.BMP_Font := BMP_Fonts.Font16x24;
        Fg : HAL.Bitmap.Bitmap_Color := To_HAL_Color(not P);
        Bg : HAL.Bitmap.Bitmap_Color := To_HAL_Color(P);
    begin
        Display.Hidden_Buffer(1).Set_Source(To_HAL_Color(P));
        Display.Hidden_Buffer(1).Fill;
        Draw_String(Display.Hidden_Buffer(1).all, (X => 100, Y => 120), "Waiting...", F, Fg, Bg);
   end Print_Waiter;
   
   procedure Print_Home(P : Grid.Player_Color) is
        F : BMP_Fonts.BMP_Font := BMP_Fonts.Font12x12;
        Fg : HAL.Bitmap.Bitmap_Color := To_HAL_Color(not P);
        Bg : HAL.Bitmap.Bitmap_Color := To_HAL_Color(P);
    begin
        Display.Hidden_Buffer(1).Set_Source(To_HAL_Color(P));
        Display.Hidden_Buffer(1).Fill;
      Draw_String(Display.Hidden_Buffer(1).all, (X => 50, Y => 100), "Press the button to", F, Fg, Bg);
      Draw_String(Display.Hidden_Buffer(1).all, (X => 50, Y => 120), "start a game as Spy.", F, Fg, Bg);
   end Print_Home;
   
   procedure Print_Start_Spy(P : Grid.Player_Color) is
        F : BMP_Fonts.BMP_Font := BMP_Fonts.Font12x12;
        Fg : HAL.Bitmap.Bitmap_Color := To_HAL_Color(not P);
        Bg : HAL.Bitmap.Bitmap_Color := To_HAL_Color(P);
    begin
        Display.Hidden_Buffer(1).Set_Source(To_HAL_Color(P));
        Display.Hidden_Buffer(1).Fill;
      Draw_String(Display.Hidden_Buffer(1).all, (X => 60, Y => 100), "Game Start!", F, Fg, Bg);
      Draw_String(Display.Hidden_Buffer(1).all, (X => 50, Y => 120), "You're the Spy.", F, Fg, Bg);
      Display.Update_Layer (1, Copy_Back => True);
   end Print_Start_Spy;
   
   procedure Print_Start_Guess(P : Grid.Player_Color) is
        F : BMP_Fonts.BMP_Font := BMP_Fonts.Font12x12;
        Fg : HAL.Bitmap.Bitmap_Color := To_HAL_Color(not P);
        Bg : HAL.Bitmap.Bitmap_Color := To_HAL_Color(P);
    begin
        Display.Hidden_Buffer(1).Set_Source(To_HAL_Color(P));
        Display.Hidden_Buffer(1).Fill;
      Draw_String(Display.Hidden_Buffer(1).all, (X => 60, Y => 100), "Game Start!", F, Fg, Bg);
      Draw_String(Display.Hidden_Buffer(1).all, (X => 50, Y => 120), "You're the Team.", F, Fg, Bg);
      Display.Update_Layer (1, Copy_Back => True);
   end Print_Start_Guess;
   
   procedure Loose_Screen(P : Grid.Player_Color) is
        F : BMP_Fonts.BMP_Font := BMP_Fonts.Font16x24;
        Fg : HAL.Bitmap.Bitmap_Color := To_HAL_Color(not P);
        Bg : HAL.Bitmap.Bitmap_Color := To_HAL_Color(P);
    begin
        Display.Hidden_Buffer(1).Set_Source(To_HAL_Color(P));
        Display.Hidden_Buffer(1).Fill;
        Draw_String(Display.Hidden_Buffer(1).all, (X => 100, Y => 120), P'Image & " lost!" , F, Fg, Bg);
    end Loose_Screen;
   
   procedure Win_Screen(P : Grid.Player_Color) is
        F : BMP_Fonts.BMP_Font := BMP_Fonts.Font16x24;
        Fg : HAL.Bitmap.Bitmap_Color := To_HAL_Color(not P);
        Bg : HAL.Bitmap.Bitmap_Color := To_HAL_Color(P);
    begin
        Display.Hidden_Buffer(1).Set_Source(To_HAL_Color(P));
        Display.Hidden_Buffer(1).Fill;
        Draw_String(Display.Hidden_Buffer(1).all, (X => 100, Y => 120), P'Image & " won!" , F, Fg, Bg);
    end Win_Screen;

    procedure Print_Selection(P : Grid.Player_Color; Current : UInt32) is
        Pt : Point := (
            X => 140,
            Y => 110
            );
        F : BMP_Fonts.BMP_Font := BMP_Fonts.Font16x24;
        Fg : HAL.Bitmap.Bitmap_Color := To_HAL_Color(not P);
        Bg : HAL.Bitmap.Bitmap_Color := To_HAL_Color(P);
    begin
        Display.Hidden_Buffer(1).Set_Source(To_HAL_Color(P));
        Display.Hidden_Buffer(1).Fill;
        Display.Hidden_Buffer(1).Set_Source(To_HAL_Color(not P));
        Draw_Line(Display.Hidden_Buffer(1).all, (X => 235, Y => 105), (X => 235, Y => 135));
        Draw_Line(Display.Hidden_Buffer(1).all, (X => 220, Y => 120), (X => 250, Y => 120));
        Draw_Line(Display.Hidden_Buffer(1).all, (X => 60, Y => 120), (X => 90, Y => 120));
        Draw_Line(Display.Hidden_Buffer(1).all, (X => 0, Y => 180), (X => 320, Y => 180));
        Draw_String(Display.Hidden_Buffer(1).all, (X => 145, Y => 200), "OK", F, Fg, Bg);
        Draw_String(Display.Hidden_Buffer(1).all, Pt, Current'Image, F, Fg, Bg);
    end Print_Selection;
end Printer;

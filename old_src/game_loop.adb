with Grid; use Grid;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;
with HAL;
with Communication;
with Printer; use Printer;
with STM32.Board; use STM32.Board;
with STM32.User_Button; use STM32;
with HAL.Touch_Panel; use HAL.Touch_Panel;
with HAL; use HAL;
with Case_Selection; use Case_Selection;

package body Game_Loop is
    function Get_Selection_Input(Game : Game_Info; Picked : in out HAL.UInt32) return Boolean is
        State : constant TP_State := STM32.Board.Touch_Panel.Get_All_Touch_Points;
        Max : UInt32;
    begin
        If Game.Current_Player = Game.First_Player then
            Max := Game.First_Player_Case;
        else
            Max := Game.Second_Player_Case;
        end if;
        case State'Length is
            when 1 =>
                if State(State'First).X > 180 then
                    return True;
                end if;
                if State(State'First).Y < 160 and Picked /= Max then
                    Picked := Picked + 1;
                else
                    if Picked /= First_Player_Hidden_Number'First then
                        Picked := Picked - 1;
                    end if;
                end if;
                return False;
            when others => return False;
        end case;
    end Get_Selection_Input;

    function "not"(W : Windows) return Windows is
    begin
        if W = Button_Window then
            return Grid_Window;
        else
            return Button_Window;
        end if;
    end "not";

    procedure Play_Spy(G : in Grid.Grid; First : Player_Color) is
        Game : Game_Info := (Current_Player => First,
        First_Player => First,
        First_Player_Case => First_Player_Hidden_Number'Last,
        Second_Player_Case => Second_Player_Hidden_Number'Last,
        Status => Picking,
        Window => Grid_Window
        );
        Picked : UInt32 := 1;
    begin
        loop
            if User_Button.Has_Been_Pressed then
                Game.Window := not Game.Window;
            end if;
            if Game.Window = Grid_Window then
                Print_Grid_Spy(G);
            else
                if Game.Status = Picking then
                    Print_Selection(Game.Current_Player, Picked);
                    if Get_Selection_Input(Game, Picked)then
                        Communication.Send_Number(Integer(Picked));
                        Game.Status := Waiting;
                    end if;
                else
                    Print_Waiter(Game.Current_Player, Picked);
                    -- Receive() modifie picked
                    if Picked = 0 then
                        Game.Status := Picking;
                        Game.Current_Player := not Game.Current_Player;
                    end if;
                end if;
         end if;
         delay 0.2;
            Display.Update_Layer (1, Copy_Back => True);
        end loop;
   end Play_Spy;

   function Grid_Input(G : in out Grid.Grid; Game : in out Game_Info; To_Find : in out UInt32) return Step is
      State : constant TP_State := STM32.Board.Touch_Panel.Get_All_Touch_Points;
      Line, Column : Grid.Grid_Index;
      C : Grid.Color;
   begin
      if Get_Selected_Case(State, Line, Column) and then not Grid.Is_Revealed(G, Line, Column) then
         C := Grid.Get_Color(G, Line, Column);
         Grid.Reveal(G, Line, Column);
         if C = Black then
            return Loose;
         end if;
         if C = White then
            return Next;
         end if;
         if C = Game.Current_Player then
            To_Find := To_Find - 1;
            if Game.Current_Player = Game.First_Player then
               Game.First_Player_Case := Game.First_Player_Case - 1;
            else
               Game.Second_Player_Case := Game.Second_Player_Case - 1;
            end if;
            if To_Find = 0 then
               return Next;
            else
               return Continue;
            end if;
         else
             if Game.Current_Player = Game.First_Player then
               Game.Second_Player_Case := Game.Second_Player_Case - 1;
            else
               Game.First_Player_Case := Game.First_Player_Case - 1;
            end if;
            return Next;
         end if;
      else
         return Continue;
      end if;
   end Grid_Input;

   procedure Play_Guess(G : in out Grid.Grid; First : Player_Color) is
       Game : Game_Info := (Current_Player => First,
        First_Player => First,
        First_Player_Case => First_Player_Hidden_Number'Last,
        Second_Player_Case => Second_Player_Hidden_Number'Last,
        Status => Picking,
        Window => Grid_Window
        );
      To_Find : UInt32 := 2;
    begin
        loop
            if User_Button.Has_Been_Pressed then
                Game.Window := not Game.Window;
            end if;
         if Game.Status = Waiting then
            Communication.Get_Nb_Turn(Integer(To_Find)); -- Blocking
            Game.Status := Picking; -- Change if Com not blocking
            if Game.Window = Grid_Window then
               Print_Grid(G);
            else
               Print_Waiter(Game.Current_Player);
            end if;
         else
            if Game.Window = Grid_Window then
               Print_Grid(G);
               case Grid_input(G, Game, To_Find) is
                  when Loose => Loose_Screen(Game.Current_Player);
                     Communication.Spy_Game_Over;
                  when Next =>
                     To_Find := (if Game.Current_Player =  Game.First_Player then
                                    Game.First_Player_Case
                                 else
                                    Game.Second_Player_Case);
                     if To_Find = 0 then
                        Win_Screen(Game.Current_Player);
                     else
                         Communication.Spy_Continue; -- Continue if no win
                     end if;
                     Game.Current_Player := not Game.Current_Player;
                     Game.Status := Waiting;
                  when Continue => null;
               end case;
            else
               Print_Waiter(Game.Current_Player, To_Find);
            end if;
         end if;
         Display.Update_Layer (1, Copy_Back => True);
         delay 0.2;
        end loop;
    end Play_Guess;
end Game_Loop;
